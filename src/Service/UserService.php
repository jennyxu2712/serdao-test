<?php

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class UserService
{
    protected $entityManager;
    protected UserRepository $userRepo;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->userRepo = $entityManager->getRepository(User::class);
    }

    public function list()
    {
        $this->_defaultData();
        return $this->userRepo->findAll();
    }

    public function _defaultData()
    {
        $usersData = [
            ['firstname' => 'Barack', 'lastname' => 'Obama', 'address' => 'White House'],
            ['firstname' => 'Britney', 'lastname' => 'Spears', 'address' => 'America'],
            ['firstname' => 'Leonardo', 'lastname' => 'DiCaprio', 'address' => 'Titanic']
        ];
        if ($this->userRepo->isTableEmpty()) {
            foreach ($usersData as $userData) {
                $this->_add($userData['firstname'], $userData['lastname'], $userData['address']);
            }
        }
    }

    public function add(Request $request)
    {
        $this->_add($request->get("firstname"), $request->get("lastname"), $request->get("address"));
    }

    private function _add($firstname, $lastname, $address)
    {
        $user = new User();
        $user->setFirstname($firstname);
        $user->setLastname($lastname);
        $user->setAddress($address);
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function delete(int $id) 
    {
        $user = $this->userRepo->find($id);
        if ($user) {
            $this->entityManager->remove($user);
            $this->entityManager->flush();
        }  
    }
}