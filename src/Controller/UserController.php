<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;
use UserService;

#[AsController]
class UserController extends AbstractController
{
    protected UserService $userService;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->userService = new UserService($entityManager);
    }

    #[Route('/user', methods: 'GET')]
    public function list(Request $request)
    {
        return $this->render('user.html.twig', [
            'users' => $this->userService->list()
        ]);
    }

    #[Route('/user', methods: 'POST')]
    public function add(Request $request)
    {
        $this->userService->add($request);
        return $this->redirect('/user');
    }

    #[Route('/user/{id}', methods: 'GET')]
    public function delete(int $id)
    {
        $this->userService->delete($id);
        return $this->redirect('/user');
    }
}